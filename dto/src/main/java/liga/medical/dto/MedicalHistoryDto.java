package liga.medical.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicalHistoryDto {

    private Long patientId;

    private String docNumber;

    private String doctor;

    private String diagnosis;

    private Long medicalHistoryParentId;
}
