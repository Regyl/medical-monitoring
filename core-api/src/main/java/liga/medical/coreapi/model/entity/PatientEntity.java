package liga.medical.coreapi.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "patient")
@EqualsAndHashCode(callSuper = true)
public class PatientEntity extends AbstractEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "age", nullable = false)
    private Integer age;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "birthday_dt", nullable = false)
    private LocalDate birthdayDt;

    @Column(name = "birth_place", nullable = false)
    private String birthPlace;

    @Column(name = "registration", nullable = false)
    private String registration;

    @Column(columnDefinition = "CHAR(6)", name = "passport_series", nullable = false)
    private Integer passportSeries;

    @Column(columnDefinition = "CHAR(4)", name = "passport_number", nullable = false)
    private Integer passportNumber;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT", name = "another_document")
    private String anotherDocument;
}
