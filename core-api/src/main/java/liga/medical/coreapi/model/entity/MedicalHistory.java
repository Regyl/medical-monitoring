package liga.medical.coreapi.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "medical_history")
@EqualsAndHashCode(callSuper = true)
public class MedicalHistory extends AbstractEntity {

    @OneToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private PatientEntity patient;

    @Column(name = "doc_number", nullable = false, unique = true)
    private String docNumber;

    @CreationTimestamp
    @Column(name = "create_dttm", nullable = false)
    private LocalDateTime createDttm;

    @UpdateTimestamp
    @Column(name = "modify_dttm", nullable = false)
    private LocalDateTime modifyDttm;

    @Column(name = "doctor", nullable = false)
    private String doctor;  //Хотя в идеале должна быть отдельна сущность, кажется

    @Column(name = "diagnosis", nullable = false)
    private String diagnosis;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private MedicalHistory medicalHistory; //По правилам нейминга должно отличаться от класса
}

