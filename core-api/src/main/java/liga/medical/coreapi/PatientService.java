package liga.medical.coreapi;

import liga.medical.coreapi.model.entity.PatientEntity;

public interface PatientService extends CrudService<PatientEntity> {
}
