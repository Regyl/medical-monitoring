package liga.medical.coreapi;

import liga.medical.coreapi.model.entity.MedicalHistory;

public interface MedicalHistoryService extends CrudService<MedicalHistory> {

}
