package liga.medical.core.core.repository;

import liga.medical.coreapi.model.entity.MedicalHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalHistoryRepository extends AbstractJpaRepository<MedicalHistory> {
}
