package liga.medical.core.core.service;

import liga.medical.core.core.repository.MedicalHistoryRepository;
import liga.medical.coreapi.MedicalHistoryService;
import liga.medical.coreapi.model.entity.MedicalHistory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

    private final MedicalHistoryRepository medicalHistoryRepository;

    public MedicalHistoryServiceImpl(MedicalHistoryRepository medicalHistoryRepository) {
        this.medicalHistoryRepository = medicalHistoryRepository;
    }

    @Override
    public MedicalHistory findById(Long id) {
        return medicalHistoryRepository.findById(id)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public List<MedicalHistory> findAll() {
        return medicalHistoryRepository.findAll();
    }

    @Override
    public MedicalHistory save(MedicalHistory entity) {
        return medicalHistoryRepository.save(entity);
    }

    @Override
    public MedicalHistory deleteById(Long id) {
        MedicalHistory medicalHistory = findById(id);
        medicalHistoryRepository.deleteById(id);
        return medicalHistory;
    }
}
