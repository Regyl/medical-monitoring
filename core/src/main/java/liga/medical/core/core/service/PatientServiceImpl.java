package liga.medical.core.core.service;

import liga.medical.core.core.repository.PatientRepository;
import liga.medical.coreapi.PatientService;
import liga.medical.coreapi.model.entity.PatientEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public PatientEntity findById(Long id) {
        return patientRepository.findById(id)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public List<PatientEntity> findAll() {
        return patientRepository.findAll();
    }

    @Override
    public PatientEntity save(PatientEntity entity) {
        return patientRepository.save(entity);
    }

    @Override
    public PatientEntity deleteById(Long id) {
        PatientEntity entity = findById(id);
        patientRepository.deleteById(id);
        return entity;
    }
}
