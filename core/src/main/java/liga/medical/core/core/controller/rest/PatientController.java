package liga.medical.core.core.controller.rest;

import liga.medical.PatientDto;
import liga.medical.core.core.mapper.PatientMapper;
import liga.medical.coreapi.PatientService;
import liga.medical.coreapi.model.entity.PatientEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/medical/patient")
public class PatientController {

    private final PatientService patientService;
    private final PatientMapper patientMapper;

    public PatientController(PatientService patientService,
                             PatientMapper patientMapper) {
        this.patientService = patientService;
        this.patientMapper = patientMapper;
    }

    @GetMapping("/")
    public List<PatientEntity> findAll() {
        return patientService.findAll();
    }

    @GetMapping("/{id}")
    public PatientEntity findById(@PathVariable Long id) {
        return patientService.findById(id);
    }

    @DeleteMapping("/{id}")
    public PatientEntity deleteById(@PathVariable Long id) {
        return patientService.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public PatientEntity create(@RequestBody PatientDto dto) {
        PatientEntity patient = patientMapper.toEntity(dto);
        return patientService.save(patient);
    }

    @PatchMapping("/{id}")
    public PatientEntity updateById(@PathVariable Long id, @RequestBody PatientDto dto) {
        PatientEntity patient = patientService.findById(id);
        patientMapper.update(patient, dto);
        return patientService.save(patient);
    }
}
