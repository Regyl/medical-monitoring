package liga.medical.core.core.mapper;

import org.modelmapper.ModelMapper;

public abstract class AbstractMapper<T, S> {

    protected final ModelMapper mapper;

    AbstractMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public abstract T toEntity(S dto);

    public abstract void update(T entity, S dto);

}
