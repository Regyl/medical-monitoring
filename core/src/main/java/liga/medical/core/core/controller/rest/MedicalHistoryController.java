package liga.medical.core.core.controller.rest;

import liga.medical.core.core.mapper.MedicalHistoryMapper;
import liga.medical.coreapi.MedicalHistoryService;
import liga.medical.coreapi.model.entity.MedicalHistory;
import liga.medical.dto.MedicalHistoryDto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/medical/history")
public class MedicalHistoryController {

    private final MedicalHistoryService medicalHistoryService;
    private final MedicalHistoryMapper medicalHistoryMapper;

    public MedicalHistoryController(MedicalHistoryService medicalHistoryService,
                                    MedicalHistoryMapper medicalHistoryMapper) {
        this.medicalHistoryService = medicalHistoryService;
        this.medicalHistoryMapper = medicalHistoryMapper;
    }

    @GetMapping("/")
    public List<MedicalHistory> findAll() {
        return medicalHistoryService.findAll();
    }

    @GetMapping("/{id}")
    public MedicalHistory findById(@PathVariable Long id) {
        return medicalHistoryService.findById(id);
    }

    @DeleteMapping("/{id}")
    public MedicalHistory deleteById(@PathVariable Long id) {
        return medicalHistoryService.deleteById(id);
    }

    @PostMapping("/")
    public MedicalHistory create(@RequestBody MedicalHistoryDto dto) {
        MedicalHistory medicalHistory = medicalHistoryMapper.toEntity(dto);
        return medicalHistoryService.save(medicalHistory);
    }

    @PatchMapping("/{id}")
    public MedicalHistory updateById(@PathVariable Long id, @RequestBody MedicalHistoryDto dto) {
        MedicalHistory medicalHistory = medicalHistoryService.findById(id);
        medicalHistoryMapper.update(medicalHistory, dto);
        return medicalHistoryService.save(medicalHistory);
    }
}
