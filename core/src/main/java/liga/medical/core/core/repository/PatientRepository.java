package liga.medical.core.core.repository;

import liga.medical.coreapi.model.entity.PatientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends AbstractJpaRepository<PatientEntity> {
}
