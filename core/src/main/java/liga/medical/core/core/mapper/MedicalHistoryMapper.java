package liga.medical.core.core.mapper;

import liga.medical.coreapi.MedicalHistoryService;
import liga.medical.coreapi.PatientService;
import liga.medical.coreapi.model.entity.MedicalHistory;
import liga.medical.coreapi.model.entity.PatientEntity;
import liga.medical.dto.MedicalHistoryDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class MedicalHistoryMapper extends AbstractMapper<MedicalHistory, MedicalHistoryDto> {

    private final PatientService patientService;
    private final MedicalHistoryService medicalHistoryService;

    public MedicalHistoryMapper(ModelMapper mapper,
                                PatientService patientService,
                                MedicalHistoryService medicalHistoryService) {
        super(mapper);
        this.patientService = patientService;
        this.medicalHistoryService = medicalHistoryService;
    }

    @Override
    public MedicalHistory toEntity(MedicalHistoryDto dto) {
        MedicalHistory medicalHistory = mapper.map(dto, MedicalHistory.class);
        setParams(dto, medicalHistory);
        return medicalHistory;
    }

    @Override
    public void update(MedicalHistory entity, MedicalHistoryDto dto) {
        mapper.map(dto, entity);
        setParams(dto, entity);
    }

    //Actions below required for each map method
    private void setParams(MedicalHistoryDto dto, MedicalHistory entity) {
        PatientEntity patient = patientService.findById(dto.getPatientId());
        MedicalHistory medicalHistoryParent = dto.getMedicalHistoryParentId() == null ?
                null : medicalHistoryService.findById(dto.getMedicalHistoryParentId());

        entity.setMedicalHistory(medicalHistoryParent);
        entity.setPatient(patient);
    }

}
