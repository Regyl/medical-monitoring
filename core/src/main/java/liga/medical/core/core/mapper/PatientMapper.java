package liga.medical.core.core.mapper;

import liga.medical.PatientDto;
import liga.medical.coreapi.model.entity.PatientEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PatientMapper extends AbstractMapper<PatientEntity, PatientDto> {

    public PatientMapper(ModelMapper mapper) {
        super(mapper);
    }

    @Override
    public PatientEntity toEntity(PatientDto dto) {
        return mapper.map(dto, PatientEntity.class);
    }

    @Override
    public void update(PatientEntity patient, PatientDto dto) {
        mapper.map(dto, patient);
    }
}
